# Sobre o RAML#

### Foi documentada a Todo-list ###

* Linguagem simples
* A documentação pode ser desenvolvida em arquivos separados
* A documentação do RAML é bem feita
* Existe um bom plugin para o atom
* A versão mais atual é a 1.0, entretando o raml2html suportado oficialmente, ainda funciona apenas para a versão 0.8
* A instalação do raml2html é bem simples, é recomendado usar o npm
* O tema do html pode ser alterado

Para gerar o html através do código raml, basta fazer download do ramltohtml(link no parágrafo "Links oficiais"), e digitar o seguinte comando:

``` ramltohtml -i arquivodeentrada.raml -o arquivodesaida.html ```

### Testes de API ###

Para testar o funcionamento das APIs, existem duas formas, utilizar uma ferramenta que "lê" a documentação e testa sozinha os status, ou então testar manualmente.
Para testar manualmente é sugerido:

* Postfix - Sugerido pelo site do Raml para testar APIs

E para testar de forma automatizada:

* Abao - Instalado com NPM, e a forma de testar é simples, basta ir no terminal e digitar algo parecido com:
    ```abao index.raml --server http:localhost:5000 ```

### Links oficiais ###

* Site Raml http://raml.org/
* Docs Raml https://github.com/raml-org/raml-spec/blob/master/versions/raml-08/raml-08.md
* Docs Raml to HTML https://github.com/raml2html/raml2html
* Docs Abao https://github.com/cybertk/abao